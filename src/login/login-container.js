import BackgroundImg from "../commons/images/animated1.gif";
import React from "react";
import {Container, Jumbotron} from "react-bootstrap";
import * as API_USERS from "./api/login-api";
import {Col, FormGroup, Input, Label, Row} from "reactstrap";
import Button from "react-bootstrap/Button";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {withRouter} from "react-router-dom";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};

const title = {color: 'white',  fontSize: 100,textAlign: 'center'};

class LoginContainer extends React.Component {

    constructor(props) {
        super(props);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,

            formControls: {
                username: {
                    value: '',
                    placeholder: 'Username',
                    touched: false,
                },
                password: {
                    value: '',
                    placeholder: 'Password',
                    touched: false,
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedControls[name] = updatedFormElement;


        this.setState({
            formControls: updatedControls,
        });

    };

    redirectUser(user) {
        return API_USERS.getUser(user,(result, status, err) => {

            if (result !== null && status === 200)
            {
                window.sessionStorage.setItem("accountID",result.linkId);
                this.props.history.push(result.usertype);

            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    handleSubmit() {
        let user = {
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value
        };

        console.log(user);
        this.redirectUser(user);
    }

    render() {
        return (
            <div>

                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                    <h1 className="display-3" style={title}>Login</h1>
                        <FormGroup id='username'>
                            <Label for='usernameField'> Username: </Label>
                            <Input name='username' id='usernameField' placeholder={this.state.formControls.username.placeholder}
                                   onChange={this.handleChange}
                                   defaultValue={this.state.formControls.username.value}
                                   touched={this.state.formControls.username.touched? 1 : 0}
                                   required
                            />
                        </FormGroup>

                        <FormGroup id='password'>
                            <Label for='passwordField'> Password: </Label>
                            <Input type="password" name='password' id='passwordField' placeholder={this.state.formControls.password.placeholder}
                                   onChange={this.handleChange}
                                   defaultValue={this.state.formControls.password.value}
                                   touched={this.state.formControls.password.touched? 1 : 0}
                                   required
                            />
                        </FormGroup>

                        <Row>
                            <Col sm={{offset: 6}}>
                                <Button className="buttonW" style={{height: '50px', width : '200px'}} type={"submit"} onClick={this.handleSubmit}> Login </Button>
                            </Col>
                        </Row>

                        {
                            this.state.errorStatus > 0 &&
                            <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                        }
                    </Container>
                </Jumbotron>
            </div>
        ) ;
    }

}

export default withRouter(LoginContainer);
