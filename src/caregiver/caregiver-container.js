import {CardHeader} from "reactstrap";
import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {Card, Col, Container, Jumbotron, Row} from "react-bootstrap";
import PatientsTable from "./components/patients-table";
import * as API_USERS from "./api/caregiver-api";
import BackgroundImg from "../commons/images/animated2.gif";
import {Client} from '@stomp/stompjs';
import {NotificationManager,NotificationContainer} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};

const title = {color: 'white',  fontSize: 100,textAlign: 'center'};

class CaregiverContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            _stompClient:null
        };
    }

    notifyCaregiver = (message) =>
    {
        return (
            NotificationManager.info(message, 'Patient Notification', 5000)
        );
    }

    componentDidMount() {

        this.fetchPatients();

        this._stompClient = new Client();

        this._stompClient.configure({

            brokerURL: 'wss://pavel-alexandru-a2-backend.herokuapp.com/pavel-30245-websocket',

            onConnect: () => {
                this._stompClient.subscribe('/topic/notify', message => {

                    this.notifyCaregiver(message.body);
                })
            }
        })

        this._stompClient.activate();
    }

    fetchPatients() {

        return API_USERS.getPatients( window.sessionStorage.getItem("accountID"),(result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchPatients();
    }

    render() {
        return (
            <div>
                <NotificationContainer/>
                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                        <h1 className="display-3" style={title}>My Patients</h1>
                <CardHeader>
                    <strong> </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 0}}>
                            {this.state.isLoaded && <PatientsTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>
                    </Container>
                </Jumbotron>
            </div>
        )

    }
}


export default CaregiverContainer;