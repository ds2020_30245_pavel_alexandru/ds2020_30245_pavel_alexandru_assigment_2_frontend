import React from "react";
import * as Moment from "moment";
import Table from "../../commons/tables/table";
import * as API_USERS from "../api/doctor-api";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import UpdatePatientForm from "./updatepatient-form";
import AddMedicationPlanForm from "./addmedicationplan-form";


const columns = [

    {
        Header: 'Id',
        accessor: 'id',
        show: false,
    },

    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'Date of birth',
        id: 'dob',
        accessor: d => {
            return Moment(d.dob)
                .local()
                .format("DD-MM-YYYY")
        }
    },
    {
        Header: 'Gender',
        accessor: 'gender',
    },
    {
        Header: 'Address',
        accessor: 'address',
    },
    {
        Header: 'Medical Record',
        accessor: 'medRecord',
    },
    {
        Cell: props=>{

            return(
                <button style={{backgroundColor: "red", color:"#fefefe"}}
                onClick={()=>{window.thisView.deletePatientId(props.original.id)}}>Delete</button>
            )
        },
        width: 100,
        maxWidth: 100,
        minWidth: 100,
    },
    {
        Cell: props=>{
            return(
                <button style={{backgroundColor: "green", color:"#fefefe"}}
                        onClick={()=>{window.thisView.toggleForm2(props.original.id,props.original.name,props.original.dob,props.original.gender,props.original.address,props.original.medRecord)}}>Update</button>
            )
        },
        width: 100,
        maxWidth: 100,
        minWidth: 100
    },
    {
        Cell: props=>{
            return(
                <button style={{backgroundColor: "darkcyan", color:"#fefefe"}}
                        onClick={()=>{window.thisView.toggleForm3(props.original.id)}}>Create Medication Plan</button>
            )
        },
        width: 200,
        maxWidth: 200,
        minWidth: 200
    }
];


const filters1 = [
    {
        accessor: 'name'
    }
];

class PatientsTable1 extends React.Component {

    constructor(props) {
        super(props);

        this.reloadHandlerTable = this.props.reloadHandlerTable;
        this.toggleForm2 = this.toggleForm2.bind(this);
        this.toggleForm3 = this.toggleForm3.bind(this);

        this.state = {

            tablePatientData: this.props.tablePatientData,
            selected2: false,
            collapseForm2: false,
            information: [],
            updatedId: [],
            selected3: false,
            idSend: []
        };
        window.thisView=this;

    }

    toggleForm2(id,name,dob,gender,address,medRecord)
    {
        let patient = {
            name:name,
            dob: dob,
            gender: gender,
            address: address,
            medRecord: medRecord,
        };

        this.state.updatedId=id;
        this.state.information=patient;

        this.setState({selected2: !this.state.selected2});
    }

    toggleForm3(id)
    {
        this.state.idSend=id;
        this.setState({selected3: !this.state.selected3});
    }

    deletePatientId(id) {
        return API_USERS.deletePatient(id,(result, status, err) => {

            if (result !== null && (status === 200 || status ===204 || status===202))
            {
                console.log("Deleted patient with id:",id);
                this.deleteUserId(id);

            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    deleteUserId(id) {
        return API_USERS.deleteUser(id,(result, status, err) => {

            if (result !== null && (status === 200 || status ===204 || status===202))
            {
                console.log("Deleted user linked to account id:",id);
                console.log(this.reloadHandlerTable);
                this.reloadHandlerTable();

            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    render() {

        return (
            <div>
            <Table
                data={this.state.tablePatientData}
                columns={columns}
                search={filters1}
                pageSize={5}
            />
                <Modal isOpen={this.state.selected2} toggle={this.toggleForm2}
                       className={this.props.className} size="lg" >
                    <ModalHeader toggle={this.toggleForm2}> Edit Patient Data: </ModalHeader>
                    <ModalBody>
                        <UpdatePatientForm information = {this.state.information} updatedId = {this.state.updatedId} reloadForUpdate = {this.reloadHandlerTable}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected3} toggle={this.toggleForm3}
                       className={this.props.className} size="lg" >
                    <ModalHeader toggle={this.toggleForm3}> Create Medication Plan: </ModalHeader>
                    <ModalBody>
                        <AddMedicationPlanForm updatedId = {this.state.idSend}/>
                    </ModalBody>
                </Modal>
            </div>
        )
    }

}

export default PatientsTable1;