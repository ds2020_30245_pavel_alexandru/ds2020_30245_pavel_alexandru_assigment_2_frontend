import React from 'react';
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/doctor-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';



class AddPatientForm extends React.Component {

    constructor(props) {
        super(props);
        this. toggleForm1 = this.toggleForm1.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,
            link_id: '',

            formControls: {
                name: {
                    value: '',
                    placeholder: 'Patient Name',
                    touched: false,
                },
                dob: {
                    value: '',
                    placeholder: 'YYYY-mm-dd',
                    touched: false,
                },
                gender: {
                    value: '',
                    placeholder: 'Patient Gender',
                    touched: false,
                },
                address: {
                    value: '',
                    placeholder: 'Patient Address',
                    touched: false,
                },
                medRecord: {
                    value: '',
                    placeholder: 'Medical History of Patient',
                    touched: false,
                },
                userName: {
                    value: '',
                    placeholder: 'Patient Account Username',
                    touched: false,
                },
                password: {
                    value: '',
                    placeholder: 'Patient Account Password',
                    touched: false,
                },

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    toggleForm1() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    registerPatient(patient) {
        return API_USERS.addPatient(patient, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted patient with id: " + result);
                let user = {
                    userName: this.state.formControls.userName.value,
                    password: this.state.formControls.password.value,
                    usertype: 'patient',
                    linkId: result,
                };

                console.log(user);

                this.registerUser(user);

            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    registerUser(user)
    {
        return API_USERS.addUser(user, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted user with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });

    }

    handleSubmit() {
        let patient = {
            name: this.state.formControls.name.value,
            dob: this.state.formControls.dob.value,
            gender: this.state.formControls.gender.value,
            address: this.state.formControls.address.value,
            medRecord: this.state.formControls.medRecord.value,
        };

        console.log(patient);

        this.registerPatient(patient);

    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedControls[name] = updatedFormElement;


        this.setState({
            formControls: updatedControls,
        });

    };

    render() {
        return (
            <div>

                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='dob'>
                    <Label for='dobField'> Date of Birth: </Label>
                    <Input name='dob' id='dobField' placeholder={this.state.formControls.dob.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.dob.value}
                           touched={this.state.formControls.dob.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='gender'>
                    <Label for='genderField'> Gender: </Label>
                    <Input name='gender' id='genderField' placeholder={this.state.formControls.gender.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.gender.value}
                           touched={this.state.formControls.gender.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='address'>
                    <Label for='addressField'> Address: </Label>
                    <Input name='address' id='addressField' placeholder={this.state.formControls.address.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.address.value}
                           touched={this.state.formControls.address.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='medRecord'>
                    <Label for='medRecordField'> Medical Record: </Label>
                    <Input name='medRecord' id='medRecordField' placeholder={this.state.formControls.medRecord.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.medRecord.value}
                           touched={this.state.formControls.medRecord.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='userName'>
                    <Label for='userNameField'> Username: </Label>
                    <Input name='userName' id='userNameField' placeholder={this.state.formControls.userName.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.userName.value}
                           touched={this.state.formControls.userName.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='password'>
                    <Label for='passwordField'> Password: </Label>
                    <Input name='password' id='passwordField' placeholder={this.state.formControls.password.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.password.value}
                           touched={this.state.formControls.password.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default AddPatientForm;
