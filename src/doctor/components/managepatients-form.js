import React from 'react';
import * as API_USERS from "../api/doctor-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import {Card, Container, Jumbotron} from "react-bootstrap";
import PatientsTable1 from "./patients-table";
import BackgroundImg from "../../commons/images/animated4.gif";
import PatientsTable2 from "./patients1-table";
import PatientsTable3 from "./patients2-table";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'repeat',
    width: "100%",
    height: "1100px",
    backgroundImage: `url(${BackgroundImg})`
};

const textStyle = {color: 'white',  fontSize: 42,textAlign: 'left'};

class ManagePatientsForm extends React.Component {

    constructor(props) {
        super(props);
        this. toggleFormPatientsManagement = this.toggleFormPatientsManagement.bind(this);
        this.reload = this.reload.bind(this);

        this.state = {
            allPatientsTable : [],
            isLoaded1 : false,
            tablePatientDataCaregiver : [],
            isLoaded2 : false,
        }
    }

    toggleFormPatientsManagement() {
        this.setState({collapseForm1: !this.state.collapseForm1});
    }

    fetchPatients() {

        return API_USERS.getPatients((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    allPatientsTable: result,
                    isLoaded2: true
                });
                console.log("Second table data load"+this.state.allPatientsTable);

            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchCaregiversPatients() {

        return API_USERS.getPatientsByCaregiver(this.props.currentId,(result, status, err) => {

            if (result !== null && status === 200) {

                this.setState({
                    tablePatientDataCaregiver: result,
                    isLoaded1: true

                });
                console.log("First table data load:"+this.state.tablePatientDataCaregiver);

            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    componentDidMount() {
        this.fetchCaregiversPatients();
        this.fetchPatients();
    }

    reload() {

        this.setState({
            isLoaded1: false,
            isLoaded2: false,
        });

        this.fetchCaregiversPatients();
        this.fetchPatients();
    }


    render() {
            return (

                <div>
                    <Jumbotron fluid style={backgroundStyle}>
                        <Container fluid>

                            <br/><h1 className="display-3" style={textStyle}>Caregiver's Patients</h1><br/>

                            <Card>
                                <Row>
                                    <Col sm={{offset: 1}}>

                                        {this.state.isLoaded1 &&
                                        <PatientsTable2 tablePatientDataCaregiver={this.state.tablePatientDataCaregiver}  idGet={this.props.currentId}
                                                        reloaderr={this.reload}/>}
                                        {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                            errorStatus={this.state.errorStatus}
                                            error={this.state.error}
                                        />}
                                    </Col>
                                </Row>
                            </Card>

                            <br/><h1 className="display-3" style={textStyle}>All Patients</h1><br/>

                            <Card>
                                <br/>
                                <Row>
                                    <Col sm={{offset: 1}}>
                                        {this.state.isLoaded2 && <PatientsTable3 allPatientsTable={this.state.allPatientsTable}
                                                                                 idGet={this.props.currentId}
                                                                                 reloaderr={this.reload}/>}
                                        {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                            errorStatus={this.state.errorStatus}
                                            error={this.state.error}
                                        />}
                                    </Col>
                                </Row>
                            </Card>

                        </Container>
                    </Jumbotron>


                </div>
            );
        }

}

export default ManagePatientsForm;
