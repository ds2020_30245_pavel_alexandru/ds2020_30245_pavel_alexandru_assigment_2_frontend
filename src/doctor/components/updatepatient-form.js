import React from 'react';
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/doctor-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';



class UpdatePatientForm extends React.Component {


    constructor(props) {
        super(props);
        this.toggleForm2 = this.toggleForm2.bind(this);
        this.reloadForUpdate = this.props.reloadForUpdate;

        this.state = {

            patientInfo: this.props.information,
            patientId: this.props.updatedId,

            errorStatus: 0,
            error: null,
            link_id: '',

            formControls: {
                name: {
                    value: this.props.information.name,
                    touched: false,
                },
                dob: {
                    value: this.props.information.dob,
                    touched: false,
                },
                gender: {
                    value: this.props.information.gender,
                    touched: false,
                },
                address: {
                    value: this.props.information.address,
                    touched: false,
                },
                medRecord: {
                    value: this.props.information.medRecord,
                    touched: false,
                },

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm2() {
        this.setState({collapseForm2: !this.state.collapseForm2});
    }

    updatePatient(id,patient) {
        return API_USERS.updatePatient(id,patient, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                this.reloadForUpdate();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let patient = {
            name: this.state.formControls.name.value,
            dob: this.state.formControls.dob.value,
            gender: this.state.formControls.gender.value,
            address: this.state.formControls.address.value,
            medRecord: this.state.formControls.medRecord.value,
        };

        console.log(patient);

        this.updatePatient(this.state.patientId,patient);
    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedControls[name] = updatedFormElement;


        this.setState({
            formControls: updatedControls,
        });

    };

    render() {
        return (
            <div>

                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.patientInfo.name}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='dob'>
                    <Label for='dobField'> Date of Birth: </Label>
                    <Input name='dob' id='dobField' placeholder={this.state.formControls.dob.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.patientInfo.dob}
                           touched={this.state.formControls.dob.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='gender'>
                    <Label for='genderField'> Gender: </Label>
                    <Input name='gender' id='genderField' placeholder={this.state.formControls.gender.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.patientInfo.gender}
                           touched={this.state.formControls.gender.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='address'>
                    <Label for='addressField'> Address: </Label>
                    <Input name='address' id='addressField' placeholder={this.state.formControls.address.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.patientInfo.address}
                           touched={this.state.formControls.address.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='medRecord'>
                    <Label for='medRecordField'> Medical Record: </Label>
                    <Input name='medRecord' id='medRecordField' placeholder={this.state.formControls.medRecord.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.patientInfo.medRecord}
                           touched={this.state.formControls.medRecord.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default UpdatePatientForm;
