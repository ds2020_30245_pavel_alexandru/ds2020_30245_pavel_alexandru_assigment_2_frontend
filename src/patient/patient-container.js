import {CardHeader} from "reactstrap";
import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {Card, Col, Container, Jumbotron, Row} from "react-bootstrap";
import * as API_USERS from "./api/patient-api";
import BackgroundImg from "../commons/images/animated3.gif";
import MedicationplanTable from "./components/medicationplan-table";
import * as Moment from "moment";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};

const title = {color: 'white',  fontSize: 100,textAlign: 'center'};
const textStyle = {color: 'white',  fontSize: 42,textAlign: 'left'};

class PatientContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            tableData: [],
            clientData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchMedicationPlans();
        this.getAccountData();
    }

    fetchMedicationPlans() {

        return API_USERS.getMedicalPlans( window.sessionStorage.getItem("accountID"),(result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    getAccountData() {

        return API_USERS.getPatient(window.sessionStorage.getItem("accountID"),(result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    clientData: result,
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.getAccountData();
        this.fetchMedicationPlans();
    }

    render() {
        Moment.locale('en');
        return (
            <div>
                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                        <h1 className="display-3" style={title}>Account Informations</h1>
                        <p className="lead" style={textStyle}>
                        <Col sm={{size: '8', offset: 0}}>
                            <br />
                           <b>Patient Name: {this.state.clientData.name}</b>
                            <br />
                            <b>Patient Date of Birth: {Moment(this.state.clientData.dob).format('DD MMM YYYY')} </b>
                            <br />
                            <b>Patient Gender: {this.state.clientData.gender}</b>
                            <br />
                            <b>Patient Address: {this.state.clientData.address}</b>
                            <br />
                            <b>Patient Medical Record: {this.state.clientData.medRecord}</b>
                            <br /><br />
                        </Col>
                        </p>
                        <h1 className="display-3" style={title}>My Medication Plan</h1>
                        <CardHeader>
                            <strong> </strong>
                        </CardHeader>
                        <Card>
                            <br/>
                            <Row>
                                <Col sm={{offset: 0}}>
                                    {this.state.isLoaded && <MedicationplanTable tableData = {this.state.tableData}/>}
                                    {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                        errorStatus={this.state.errorStatus}
                                        error={this.state.error}
                                    />   }
                                </Col>
                            </Row>
                        </Card>
                    </Container>
                </Jumbotron>
            </div>
        )

    }
}


export default PatientContainer;